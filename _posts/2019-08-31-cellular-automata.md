---
layout: inner
position: left
title: 'Cellular Automata'
date: 2019-08-31 08:00:00
categories: development
tags: C++ Qt
featured_image: '/img/posts/02-cellular-automata.png'
project_link: 'https://gitlab.com/coppercoder/game-of-life'
button_icon: 'fab fa-gitlab'
button_text: 'Visit Project'
lead_text: "A simulator for <a href=\"https://en.wikipedia.org/wiki/Conway's_Game_of_Life\">Conway's Game of Life</a> and other related <a href=\"https://en.wikipedia.org/wiki/Cellular_automaton\">cellular automata</a>."
---
