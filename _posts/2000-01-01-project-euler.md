---
layout: inner
position: right
title: 'Project Euler'
date: 2000-01-01 08:00:00 # last in the list
categories: development
tags: Python Math Challenge
featured_image: '/img/posts/08-project-euler.png'
project_link: 'https://gitlab.com/coppercoder/project-euler'
button_icon: 'fab fa-gitlab'
button_text: 'Visit Project'
lead_text: 'Solutions to 120+ <a href="http://projecteuler.net">Project Euler</a> math and programming problems, including problems at 100% and 85% difficulty.'
---
