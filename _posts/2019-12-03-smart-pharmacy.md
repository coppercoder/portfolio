---
layout: inner
position: right
title: 'Smart Pharmacy'
date: 2019-12-3 08:00:00
categories: development
tags: Java JavaFX JavaScript Firebase Python Face~Recognition Raspberry~Pi
featured_image: '/img/posts/03-smart-pharmacy.png'
lead_text: "An end-to-end medication dispensing system which authenticates users via face recognition, including a patient management application, Firebase Cloud Functions for face matching, and a Python script on a Raspberry Pi which scans users' faces and dispenses medication."
---
